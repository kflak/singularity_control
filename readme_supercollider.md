# Singularity SuperCollider

The SuperCollider code is responsible for sound, lights and controlling the openFrameworks program. The light is controlled with an Enttec DMXUSB PRO interface connected with the theater's dimmers.

## Installing SuperCollider
On Arch Linux: 
```bash
sudo pacman -S supercollider
```

## Dependencies
In order for the code to run, the following quarks needs to be installed in SuperCollider:
- [DMX](https://github.com/supercollider-quarks/dmx)
- [CuePlayer](https://github.com/dathinaios/CuePlayer)
- [MiniBeeUtils](https://gitlab.com/kflak/minibeeutils)
- [KFUtils](https://gitlab.com/kflak/)

Install them by running this code in the SuperColider IDE: 
```javascript
Quarks.install("CuePlayer");
Quarks.install("DMX");
Quarks.install("https://gitlab.com/kflak/minibeeutils")
Quarks.install("https://gitlab.com/kflak/kfutils")
```

## Running the Code
- Evaluate `init.scd` to initialize the basic parameters. You may have to change the path of the Enttec interface in `lightCues.scd` to match yhour system.
- Open up `cues.scd` in scide or similar and evaluate the lines as needed.
